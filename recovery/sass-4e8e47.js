"use strict";

const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const fiber = require('fibers');
const {src, dest} = require('gulp');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');

const paths = require('./settings.js');

const path_src = paths.css.src;
const path_dest = paths.css.dest;

const scss_modules = [
  "node_modules/baguettebox.js/src",
  "node_modules/foundation-sites/scss",
  "node_modules/motion-ui/src"
];

const browser_support = ['last 2 versions', 'ie >= 11'];

sass.compiler = require('sass');

const css = () => {
  return src(path_src)
    .pipe(sassGlob())
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
        fiber: fiber,
        includePaths: scss_modules,
        outputStyle: "expanded"
      }).on('error', sass.logError)
    )
    .pipe(dest(path_dest))
    .pipe(rename({ suffix: ".min" }))
    .pipe(postcss([autoprefixer({ browsers: browser_support }),cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(path_dest));
}

module.exports = css;
