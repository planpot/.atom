'use strict';

const { src, dest } = require('gulp');

const paths = require('./settings.js');

const path_src = paths.font.src;
const path_dest = paths.font.dest;

const font = () =>{
  return src(path_src)
    .pipe(dest(path_dest));
}

module.exports = font;
