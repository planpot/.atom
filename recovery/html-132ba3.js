'use strict';

const {src, dest} = require('gulp');
const ejs = require('gulp-ejs');
const htmlmin = require('gulp-htmlmin');
const plumber = require('gulp-plumber');

const paths = require('./settings.js');

const pathSrc = paths.html.src;
const pathDest = paths.html.dest;

const html = () => {
  return src(pathSrc)
    .pipe(plumber())
    .pipe(ejs('',{ "ext": '.html' }))
    .pipe(htmlmin({
      collapseWhitespace: true,
      preserveLineBreaks: true
    }))
    .pipe(dest(pathDest));
}

module.exports = html;
