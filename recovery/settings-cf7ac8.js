'use strict';

const src = 'src/';
const dist = 'dist/';

const paths = {
  src: src,
  dist: dist,
  host: {
    url: 'bellsonica.lc',
    root: './dist/',
  },
  css: {
    src: `${src}scss/*.scss`,
    watch: `${src}scss/**/*.scss`,
    dest: `${dist}styles`,
  },
  js: {
    src: `${src}js/app.js`,
    watch: `${src}js/**/*.js`,
    dest: `${dist}scripts`,
  },
  html: {
    src: [
      `${src}theme/**/*.php`,
      `${src}theme/**/*.ejs`,
      `${src}theme/*.html`,
      `!${src}theme/**/_*`,
    ],
    watch: [
      `${src}theme/**/*.php`,
      `${src}theme/**/*.ejs`,
    ],
    dest: `${dist}`,
  },
  other: {
    src: [
      `${src}theme/**/*`,
      `!${src}theme/_*/*`,
      `!${src}theme/**/_*`,
      `!${src}theme/**/*.php`,
      `!${src}theme/**/*.ejs`,
    ],
    watch: [
      `${src}theme/**/*`,
      `!${src}theme/**/*.php`,
      `!${src}theme/**/*.ejs`,
    ],
    dest: `${dist}`,
  },
  image: {
    src: `${src}images/**/*`,
    watch: `${src}images/**/*`,
    dest: `${dist}images`,
  },
  font: {
    src: `${src}fonts/**/*`,
    watch: `${src}fonts/**/*`,
    dest: `${dist}fonts`,
  },
};

module.exports = paths;
